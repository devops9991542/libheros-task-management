export interface Task {
    _id?: string;
    name: string;
    description?: string;
    dueDate: Date;
    completed: boolean;
}
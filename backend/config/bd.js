const mongoose = require('mongoose');

const DB_URI = 'mongodb+srv://Hunter:Hunting@cluster0.7zjgbh9.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('MongoDB connected successfully !');
}).catch(err => {
    console.error('MongoDB connection error', err);
});

module.exports = mongoose;
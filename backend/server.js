const express = require('express');
const mongoose = require('./config/bd');
const userRoutes = require('./routes/userRoutes');
const taskRoutes = require('./routes/taskRoutes');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 3000;


//mongoose();

app.use(cors());


app.use(express.json()); // same as bodyParser.json

app.use('/api/users', userRoutes);
app.use('/api/tasks', taskRoutes);


app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
const Task = require('../models/task');

exports.createTask = async (req, res) => {
    const { name, description, dueDate, user } = req.body;
    try {
        const newTask = new Task({
            name,
            description,
            dueDate,
            user 
        });

        await newTask.save();
        res.status(201).send(newTask);
    } catch (error) {
        res.status(500).send(error);
    }
};

exports.getTasks = async (req, res) => {
    try {
        const tasks = await Task.find({ user: req.userId });
        res.status(200).send(tasks);
    } catch (error) {
        res.status(500).send(error);
    }
};

exports.getTaskById = async (req, res) => {
    try {
      const task = await Task.findById(req.params.id);
      if (!task) return res.status(404).send('Task not found');
      res.json(task);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  };

exports.updateTask = async (req, res) => {
    const { completed } = req.body;
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, { completed }, { new: true });
        res.status(200).send(task);
    } catch (error) {
        res.status(500).send(error);
    }
};

exports.deleteTask = async (req, res) => {
    try {
        await Task.findByIdAndRemove(req.params.id);
        res.status(204).send({ message: "Tâche supprimée !" });
    } catch (error) {
        res.status(500).send(error);
    }
};
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.register = async (req, res) => {
    const { username, email, password } = req.body;
    try {
        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({ username, email, password: hashedPassword });
        await newUser.save();
        res.status(201).send({ message: "Inscription réussie !" });
    } catch (error) {
        res.status(500).send(error);
    }
};




exports.login = async (req, res) => {

    const { username, password } = req.body;
    
    try {
        const user = await User.findOne({ username });
        if (!user) return res.status(404).send({ message: "User not found!" });

        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) return res.status(401).send({ message: "mot de passe invalide!" });

        const token = jwt.sign({ id: user._id }, 'RANDOM_TOKEN_SECRET', { expiresIn: 86400 });
        res.status(200).send({
            id: user._id,
            username: user.username,
            email: user.email,
            accessToken: token
        });

    } catch (error) {
        res.status(500).send(error);
    }
};
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    const token = req.headers['authorization'];

    if (!token) {
        return res.status(403).send({ message: "Token non fourni !" });
    }

    jwt.verify(token, 'RANDOM_TOKEN_SECRET', (err, decoded) => {
        if (err) {
            return res.status(401).send({ message: "Non autirisé " });
        }
        req.userId = decoded.id;
        next();
    });
};

module.exports = verifyToken;
